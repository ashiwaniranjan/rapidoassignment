import java.util.*;

public class ExamDetail {

    static void parseExamDetails(Map<String, String> examDetails) {
        System.out.println("Entrance Exam \t Registration Number \t HallTicket Number  ");
        examDetails.forEach((key, value)->{
            String[] message = value.split(" ");
            Integer registrationIndex = Integer.MIN_VALUE;
            Integer hallIndex = Integer.MIN_VALUE;
            String registrationNo = null;
            String hallNo = null;
            String firstTemp = null;
            String secondTemp = null;
            Boolean firstNoAssign = Boolean.FALSE;
            for (int i = 0; i < message.length; i++) {
                if (message[i].equalsIgnoreCase("application") || message[i].equalsIgnoreCase("registration")) {
                    registrationIndex = i;
                }
                if (message[i].equalsIgnoreCase("hall")) {
                    hallIndex = i;
                }

                if (message[i].matches((".*[0-9].*"))) {

                    if (!firstNoAssign) {
                        firstTemp = message[i];
                        firstNoAssign = Boolean.TRUE;

                    } else {
                        secondTemp = message[i];
                    }
                }

                if (registrationIndex < hallIndex) {
                    registrationNo = firstTemp;
                    hallNo = secondTemp;
                } else {
                    hallNo = firstTemp;
                    registrationNo = secondTemp;
                }


            }
            System.out.println(key+"\t"+registrationNo + "\t" + hallNo);
        });
    }
    public static void main(String[] args) {
        Map<String, String> examDetails = new LinkedHashMap<>();
        examDetails.put("JEE", "Your application has been accepted and and your registration number is 093467 and your hall ticket number is BNG32016");
        examDetails.put("VIT", "Application number 9823019348 has been accepted. 0955693 is your hall ticket number");
        examDetails.put("NEET", "Hall ticket is generated with number 39458 for the application AB123XZ");
        parseExamDetails(examDetails);

    }
}